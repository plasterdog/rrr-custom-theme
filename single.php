<?php
/**
 * The Template for displaying all single posts.
 *
 * @package plasterdog
 */

get_header(); ?>

<?php
  $post = $wp_query->post;
  if (in_category('books')) {
      include(get_template_directory().'/single-books.php');
  } 
    else{
      include(get_template_directory().'/single-default.php');
  }
?>