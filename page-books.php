<?php
/**
 * Template Name:Book Array
 *
 *
 * @package plasterdog
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<h1 class="page-title"><?php the_title(); ?></h1>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'plasterdog' ),
				'after'  => '</div>',
			) );
		?>
	<?php edit_post_link( __( 'Edit', 'plasterdog' ), '<footer class="entry-footer"><span class="edit-link">', '</span></footer>' ); ?>

			<?php endwhile; // end of the loop. ?>

<!-- QUERY FOR BOOKS CATEGORY DISPLAY  -->
			 <?php
			global $post;
			$args = array( 'numberposts' => -1, 'offset'=> 0, 'category_name' => 'books', 'orderby' => 'post_date', 'order' => 'DSC');
			$myposts = get_posts( $args );
			foreach( $myposts as $post ) :	setup_postdata($post); ?>	

			<?php if (!empty($post->post_excerpt)) : ?>
			<div class="left_picture">	
			<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_post_thumbnail( 'medium' ); ?></a>
			</div>
			<div class="right_text">
			<h2><?php the_title(); ?></h2>
			<?php the_excerpt(); ?>
			<p align="right"><a href="<?php the_permalink(); ?>" rel="bookmark">... find out more</a></p>
			</div>
			<div class="clear"><hr/></div>
			<?php else : ?>
			<div class="left_picture">	
			<?php the_post_thumbnail( 'medium' ); ?>
			</div>
			<div class="right_text">	
			<h2><?php the_title(); ?></h2>
			<?php the_content(); ?>
			</div>
			<div class="clear"><hr/></div>

			<?php endif; ?>	          
			
			<?php endforeach; ?>

</div><!-- .entry-content -->
	</main><!-- #main -->
		</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
