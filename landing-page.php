<?php
/*
*Template Name: Landing Page
 * @package plasterdog
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<h1 class="page-title"><?php the_title(); ?></h1>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'plasterdog' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
	<?php edit_post_link( __( 'Edit', 'plasterdog' ), '<footer class="entry-footer"><span class="edit-link">', '</span></footer>' ); ?>
</article><!-- #post-## -->




			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

	<div id="secondary" class="widget-area front-book-array" role="complementary">

<?php if(get_field('landing_page_sidebar_title')) {?>
	<h2><a href="<?php the_field('landing_page_sidebar_title_link'); ?>"><?php the_field('landing_page_sidebar_title'); ?></a></h2>
<?php } ?><!-- ends the first condition -->
<?php if(!get_field('landing_page_sidebar_title')) {?>		
<?php }?> <!-- ends the second outer condition -->

			 <?php
			global $post;
			$args = array( 'numberposts' => 3, 'offset'=> 0, 'category_name' => 'books', 'orderby' => 'post_date', 'order' => 'DSC');
			$myposts = get_posts( $args );
			foreach( $myposts as $post ) :	setup_postdata($post); ?>	
<li>
<div class="clear">						
<div class="left_picture">	
<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_post_thumbnail( 'medium' ); ?></a>
</div><!-- ends left picture -->
<div class="right_text">
<a href="<?php the_permalink(); ?>" rel="bookmark"><h3><?php the_title(); ?></h3></a>


<?php $trimexcerpt = get_the_excerpt();
$shortexcerpt = wp_trim_words( $trimexcerpt, $num_words = 30, $more = '… ' ); 
echo  $shortexcerpt ; 
?>


</div><!-- ends right text -->
</div><!-- ends clear -->			
</li>
			        
			
			<?php endforeach; ?>
	</aside>
	</div><!-- #secondary -->

<?php get_footer(); ?>
