<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package plasterdog
 */
?>

	</div><!-- #content -->
</div><!-- #page -->
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div id="foot-constraint">
			<div class="footer-section"><?php if(get_option('pdog_phone') && get_option('pdog_phone') != '') {?>
			<span class="icon-text"><?php echo get_option('pdog_phone') ?></span>  <?php } ?>	
			<?php if(!get_option('pdog_phone')) {?>	&nbsp;		<?php }?>
			</div>

			<div class="footer-section">
			<?php if(get_option('pdog_address') && get_option('pdog_address') != '') {?>
			<span class="icon-text"><?php echo get_option('pdog_address') ?></span>  <?php } ?>
			<?php if(!get_option('pdog_address')) {?>	&nbsp;		<?php }?>
			</div>

			<div class="footer-section">
	&copy; <?php $the_year = date("Y"); echo $the_year; ?> | <?php bloginfo( 'name' ); ?> | All rights reserved
			</div>
			<div class="site-info">
			</div><!-- .site-info -->
		</div>

		
	</footer><!-- #colophon -->


<?php wp_footer(); ?>

</body>
</html>
