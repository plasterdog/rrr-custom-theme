<?php
/**
 * The Template for displaying all single posts.
 *
 * @package plasterdog
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>


			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="left_picture">	
			<?php the_post_thumbnail( 'medium' ); ?>
			</div>
<div class="right_text">
	<header class="entry-header">
		<h1 class="entry-title"><?php the_title(); ?></h1>

		<div class="entry-meta">
		
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>


	</div><!-- .entry-content -->

	<footer class="entry-footer">
		

		<?php edit_post_link( __( 'Edit', 'plasterdog' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->

</div>

</article><!-- #post-## -->

				<div class="clear"><!-- variation from default nav which restricts navigation within category -->
<div class="left-split-nav"><?php previous_post_link('%link', '&larr; %title', TRUE) ?></div>
<div class="right-split-nav"><?php next_post_link('%link', '%title &rarr;', TRUE) ?></div>
</div>

			<?php
				// If comments are open or we have at least one comment, load up the comment template
				if ( comments_open() || '0' != get_comments_number() ) :
					comments_template();
				endif;
			?>

		<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>